package Renderer;

import Utils.Lerp;
import model.Part;
import model.Scene;
import model.Solid;
import model.Vertex;
import raster.LineRasterizer;
import raster.TriangleRasterizer;
import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Renderer {

    private Mat4 view, projection;
    private TriangleRasterizer triangleRasterizer;
    private LineRasterizer lineRasterizer;
    private final Lerp<Vertex> lerp;
    private Scene scene;
    private Boolean renderAll = true;

    private int selectedSolid = 0;
    private int selectedColor = 0;
    boolean renderingTriangle = false;

    private Part part;
    private Solid solid;

    private ArrayList<Col> colors = new ArrayList<>();
    private boolean oneColorTriangle = true; // Urcuje jestli vykresli jenom jednobarevne nebo multibarevne

    public Renderer(TriangleRasterizer triangleRasterizer, LineRasterizer lr, Scene scene) {
        this.triangleRasterizer = triangleRasterizer;
        this.lineRasterizer = lr;
        this.lerp = new Lerp<>();
        colors.add(new Col(Color.GREEN.getRGB()));
        colors.add(new Col(Color.YELLOW.getRGB()));
        colors.add(new Col(Color.RED.getRGB()));
        colors.add(new Col(Color.BLUE.getRGB()));
        this.scene = scene;
    }

    public void render(Solid solid){
        for(Part part : solid.getPartBuffer()) {
            switch(part.getType())
            {
                case LINE: {
                    int start = part.getIndex();
                    for(int i =0; i< part.getCount();i++) {
                        int indexA = start;
                        int indexB = start + 1;
                        start += 2;

                        Vertex a = solid.getVertexBuffer().get(solid.getIndexBuffer().get(indexA));
                        Vertex b = solid.getVertexBuffer().get(solid.getIndexBuffer().get(indexB));

                        renderLine(a.transform(scene.getModelMats().get(selectedSolid)).transform(getView()).transform(getProjection()), b.transform(scene.getModelMats().get(selectedSolid)).transform(getView()).transform(getProjection()));
                    }
                } break;
                case TRIANGLE: {
                    if (renderAll) {
                        int start = part.getIndex();
                        for (int i = 0; i < part.getCount(); i++) {
                            if (selectedColor > 3) {
                                selectedColor = 0;
                            }
                            int indexA = start;
                            int indexB = start + 1;
                            int indexC = start + 2;
                            start += 3;

                            Vertex a = solid.getVertexBuffer().get(solid.getIndexBuffer().get(indexA));
                            Vertex b = solid.getVertexBuffer().get(solid.getIndexBuffer().get(indexB));
                            Vertex c = solid.getVertexBuffer().get(solid.getIndexBuffer().get(indexC));
                            renderTriangle(a.transform(scene.getModelMats().get(selectedSolid)).transform(getView()).transform(getProjection()),
                                    b.transform(scene.getModelMats().get(selectedSolid)).transform(getView()).transform(getProjection()),
                                    c.transform(scene.getModelMats().get(selectedSolid)).transform(getView()).transform(getProjection()),
                                    colors.get(selectedColor));
                            selectedColor++;
                        }
                    }
                }break;
            }
        }
    }

    private boolean isTriangleClipped(Vertex a, Vertex b, Vertex c){
        return (-a.getPosition().w > a.getX() && -b.getPosition().w > b.getX() && -c.getPosition().w > c.getX()) ||
                (a.getX() > a.getPosition().w && b.getX() > b.getPosition().w && c.getX() > c.getPosition().w) ||
                (-a.getPosition().w > a.getY() && -b.getPosition().w > b.getY() && -c.getPosition().w > c.getY()) ||
                (a.getY() > a.getPosition().w && b.getY() > b.getPosition().w && c.getY() > c.getPosition().w) ||
                (0 > a.getZ() && 0 > b.getZ() && 0 > c.getZ()) ||
                (a.getZ() > a.getPosition().w && b.getZ() > b.getPosition().w && c.getZ() > c.getPosition().w);
    }

    private boolean isLineClipped(Vertex a, Vertex b){
            return (-a.getPosition().w > a.getX() && -b.getPosition().w > b.getX()) ||
                    (a.getX() > a.getPosition().w && b.getX() > b.getPosition().w) ||
                    (-a.getPosition().w > a.getY() && -b.getPosition().w > b.getY()) ||
                    (a.getY() > a.getPosition().w && b.getY() > b.getPosition().w) ||
                    (0 > a.getZ() && 0 > b.getZ()) ||
                    (a.getZ() > a.getPosition().w && b.getZ() > b.getPosition().w);
    }

    private void renderTriangle(Vertex a, Vertex b, Vertex c, Col color){
        //TODO: fast clip
        if(isTriangleClipped(a,b,c)) return;

        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }
        if (b.getZ() < c.getZ()) {
            Vertex temp = b;
            b = c;
            c = temp;
        }
        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }

        double zMin = 0;
        if(a.getZ() < zMin) {
            return;

        }
        else if(b.getZ() < zMin)
        {
            double t = (0-a.getZ())/(b.getZ()-a.getZ());
            Vertex vab = lerp.lerp(a,b,t);
            double t2 = (0-a.getZ())/(c.getZ()-a.getZ());
            Vertex vac = lerp.lerp(a,c,t2);

            triangleRasterizer.rasterize(a,
                    vab,
                    vac, oneColorTriangle,color);
        }
        else if(c.getZ() < zMin){
            double t = (0-a.getZ())/(c.getZ()-a.getZ());
            Vertex vac = lerp.lerp(a,c,t);
            double t2 = (0-b.getZ())/(c.getZ()-b.getZ());
            Vertex vbc = lerp.lerp(b,c,t2);
            triangleRasterizer.rasterize(a,b,vac, oneColorTriangle,color);
            triangleRasterizer.rasterize(b,vac,vbc, oneColorTriangle,color);
        }
        else {
            triangleRasterizer.rasterize(a, b, c, oneColorTriangle,color);
        }
    }

    private void renderLine(Vertex a, Vertex b){
        if(isLineClipped(a,b)) return;

        if (a.getZ() < b.getZ()) {
            Vertex temp = a;
            a = b;
            b = temp;
        }
            lineRasterizer.rasterize(a, b,new Col(a.getColor().add(b.getColor())));
    }

    public void render(List<Solid> scene){
        for (Solid s:scene) {
            render(s);
            oneColorTriangle = !oneColorTriangle;
            selectedSolid++;
        }
        oneColorTriangle = true;
        selectedSolid = 0;
    }

    public Mat4 getView() {
        return view;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    public void setScene(Scene scene){
        this.scene = scene;
    }

    public void setRenderType(){
        renderAll = !renderAll;
    }
}
