package control;

import Renderer.Renderer;
import model.*;
import raster.*;
import transforms.*;
import view.Panel;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class Controller3D implements Controller {
    private final ZBuffer zBuffer;
    private final Panel panel;
    private final Renderer renderer;
    private Camera camera;
    private Mat4 projection;
    private boolean perspective = true;
    private TriangleRasterizer triangleRasterizer;
    private int x, y;
    private int choice = 0;
    Mat4 pyramidMat = new Mat4();
    Mat4 cubeMat = new Mat4();

    Mat4Identity mX;
    Mat4Identity mY;
    Mat4Identity mZ;
    Mat4Transl mTransl;
    Mat4Scale mScale;
    boolean axis = false;

    List<Double> rotX = new ArrayList<>();
    List<Double> rotY = new ArrayList<>();
    List<Double> rotZ = new ArrayList<>();
    List<Double> posX = new ArrayList<>();
    List<Double> posY = new ArrayList<>();
    List<Double> posZ = new ArrayList<>();

    private Scene scene = new Scene();
    private LineRasterizer lineRasterizer;

    public Controller3D(Panel panel) {
        this.panel = panel;
        this.zBuffer = new ZBuffer(panel.getRaster());
        this.triangleRasterizer = new TriangleRasterizer(zBuffer);
        this.lineRasterizer = new LineRasterizer(zBuffer);
        renderer = new Renderer(triangleRasterizer,lineRasterizer,scene);

        initPos();
        initRot();
        addSolidsToScene();
        initE();
    }

    public void initE(){
        initCamera();
        initPersp();
        initRenderer();
        initObjects(panel.getRaster());
        initListeners();
        renderScene();
    }

    //pocatecni Rotace pro Solidy
    public void initRot(){
        rotX.add(1.0); rotY.add(-0.3); rotZ.add(0.1);
        rotX.add(0.1); rotY.add(0.1); rotZ.add(0.1);
        rotX.add(0.1); rotY.add(0.2); rotZ.add(-0.7);
    }

    //Pocatecni pozice pro solidy
    public void initPos(){
        posX.add(0d); posY.add(0d); posZ.add(0d); //Pyramid
        posX.add(1d); posY.add(1d); posZ.add(1d); //Cube
        posX.add(3d); posY.add(0d); posZ.add(0d); //Pyramid
    }

    public void addSolidsToScene(){

        scene.addSolid(new Pyramid());
        scene.addSolid(new Cube());
        scene.addSolid(new Pyramid());
        for(int i = 0; i < scene.getSolids().size();i++){
            scene.setModelMat(i, new Mat4Identity().mul(new Mat4RotX(rotX.get(i)))
                    .mul(new Mat4RotY(rotY.get(i)))
                    .mul(new Mat4RotZ(rotZ.get(i)))
                    .mul(new Mat4Transl(posX.get(i),posY.get(i),posZ.get(i))));
        }
        if(axis) {
            scene.addSolid(new XYZAxis());
        }
    }

    public void initObjects(ImageBuffer raster) {
        raster.setClearValue(new Col(0x101010));
    }

    @Override
    public void initListeners() {
        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
            }
        });
        panel.addMouseMotionListener(new MouseAdapter(){

            @Override
            public void mouseDragged(MouseEvent e){

                double dc = x-e.getX();
                double dr = y-e.getY();
                x = e.getX();
                y = e.getY();
                camera.addAzimuth(dc/panel.getWidth());
                camera.addZenith(dr/panel.getHeight());
                cameraMoved();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                x = e.getX();
                y = e.getY();
            }
        });
        panel.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_A -> {
                        camera.left(0.5d);
                        cameraMoved();
                    }
                    case KeyEvent.VK_D -> {
                        camera.right(0.5d);
                        cameraMoved();
                    }
                    case KeyEvent.VK_W -> {
                        camera.up(0.5d);
                        cameraMoved();
                    }
                    case KeyEvent.VK_S -> {
                        camera.down(0.5d);
                        cameraMoved();
                    }
                    case KeyEvent.VK_E -> {
                        camera.forward(0.5);
                        cameraMoved();
                    }
                    case KeyEvent.VK_R -> {
                        camera.backward(0.5);
                        cameraMoved();
                    }

                    //ROTACE X
                    case KeyEvent.VK_I -> rotateX(0.1d);
                    case KeyEvent.VK_U -> rotateX(-0.1);


                    //ROTACE Y
                    case KeyEvent.VK_K -> rotateY(0.1);
                    case KeyEvent.VK_J -> rotateY(-0.1);


                    //ROTACE Z
                    case KeyEvent.VK_N -> rotateZ(-0.1);
                    case KeyEvent.VK_M -> rotateZ(0.1);
                    case KeyEvent.VK_L -> {
                        scene = new Scene();
                        axis = !axis;
                        addSolidsToScene();
                        cameraMoved();
                    }


                    //Translace
                    case KeyEvent.VK_F -> translX(0.1);
                    case KeyEvent.VK_H -> translX(-0.1);
                    case KeyEvent.VK_T -> translY(0.1);
                    case KeyEvent.VK_G -> translY(-0.1);
                    case KeyEvent.VK_V -> translZ(0.1);
                    case KeyEvent.VK_B -> translZ(-0.1);
                    case KeyEvent.VK_Q -> {
                        renderer.setRenderType();
                        cameraMoved();
                    }

                    //Vyber s cim manipulovat
                    case KeyEvent.VK_F1 -> {
                        choice = 0;
                        getPosition();
                        getRotations();
                    }
                    case KeyEvent.VK_F2 -> {
                        choice = 1;
                        getPosition();
                        getRotations();
                    }
                    case KeyEvent.VK_F3 -> {
                        choice = 2;
                        getPosition();
                        getRotations();
                    }

                    //Zmena perspektivy
                    case KeyEvent.VK_P -> {
                        projection = new Mat4PerspRH(Math.PI / 4, 1, 0.1, 200);
                        cameraMoved();
                    }
                    case KeyEvent.VK_O -> {
                        projection = new Mat4OrthoRH(4, 4, 0.1, 200);
                        cameraMoved();
                    }
                    default -> {
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    private void renderScene() {
        panel.clear();
        renderer.setScene(this.scene);
        renderer.render(this.scene.getSolids());
        panel.repaint();
    }

    private void cameraMoved(){
        initRenderer();
        renderScene();
    }

    private void initCamera()
    {
        this.camera = new Camera();
        this.camera.setPosition(new Vec3D(0.5, -6, 2));
        this.camera.addAzimuth(Math.toRadians(90));
        this.camera.addZenith(Math.toRadians(-20));
    }

    private void initPersp(){
        if(perspective) projection = new Mat4PerspRH(Math.PI / 4, panel.getHeight() / (float) panel.getWidth(), 1, 200);
        else projection = new Mat4OrthoRH(18, 8, 1, 50);
    }

    private void initRenderer(){
        this.zBuffer.clear();
        this.triangleRasterizer = new TriangleRasterizer(zBuffer);
        this.lineRasterizer = new LineRasterizer(zBuffer);
        this.renderer.setView(this.camera.getViewMatrix());
        this.renderer.setProjection(this.projection);
    }

    public void getPosition(){
        mTransl = new Mat4Transl(posX.get(choice)+0.1,posY.get(choice),posZ.get(choice));
    }
    //Získá rotace právě vybraného tělesa
    public void getRotations(){
        mX = new Mat4RotX(this.rotX.get(choice));
        mY = new Mat4RotY(this.rotY.get(choice));
        mZ = new Mat4RotZ(this.rotZ.get(choice));
    }

    public void rotateX(double rotX){
        getRotations();
        getPosition();
        
        this.rotX.set(choice,this.rotX.get(choice) + rotX);
        mX = new Mat4RotX(this.rotX.get(choice));
        scene.setModelMat(choice,new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }
    public void rotateY(double rotY){
        getRotations();
        getPosition();
        
        this.rotY.set(choice,this.rotY.get(choice) + rotY);
        mY = new Mat4RotY(this.rotY.get(choice));
        scene.setModelMat(choice,new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }
    public void rotateZ(double rotZ){
        getRotations();
        getPosition();
        
        this.rotZ.set(choice,this.rotZ.get(choice) + rotZ);
        mZ = new Mat4RotZ(this.rotZ.get(choice));
        scene.setModelMat(choice,new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }

    public void translX(double transl){
        getRotations();
        getPosition();
        
        mTransl = new Mat4Transl(posX.get(choice)+transl,posY.get(choice),posZ.get(choice));
        posX.set(choice,posX.get(choice)+transl);
        scene.setModelMat(choice, new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }
    public void translY(double transl){
        getRotations();
        getPosition();
        
        mTransl = new Mat4Transl(posX.get(choice),posY.get(choice)+transl,posZ.get(choice));
        posY.set(choice,posY.get(choice)+transl);
        scene.setModelMat(choice, new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }
    public void translZ(double transl){
        getRotations();
        getPosition();
        
        mTransl = new Mat4Transl(posX.get(choice),posY.get(choice),posZ.get(choice)+transl);
        posZ.set(choice,posZ.get(choice)+transl);
        scene.setModelMat(choice, new Mat4Identity().mul(mX).mul(mY).mul(mZ).mul(mTransl));
        cameraMoved();
    }
}
