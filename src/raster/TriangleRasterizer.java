package raster;
import Utils.Lerp;
import model.Vertex;
import transforms.Col;

public class TriangleRasterizer {
    private final ZBuffer zBuffer;
    private final Lerp<Vertex> lerp;
    private final int width, height;

    public TriangleRasterizer(ZBuffer zBuffer) {
        this.zBuffer = zBuffer;
        this.width = zBuffer.getWidth();
        this.height = zBuffer.getHeight();
        this.lerp = new Lerp<>();
    }

    public void rasterize(Vertex p1, Vertex p2, Vertex p3, boolean oneColorTriangle, Col color) {

        Vertex a = p1.dehomog();
        Vertex b = p2.dehomog();
        Vertex c = p3.dehomog();

        //Aby fungoval ve vertexu transformToWindow
        a.setResolution(width, height);
        b.setResolution(width, height);
        c.setResolution(width, height);

        a = a.transformToWindow(a.getPosition());
        b = b.transformToWindow(b.getPosition());
        c = c.transformToWindow(c.getPosition());

        while (!(a.getY() <= b.getY() && b.getY() <= c.getY())) {
            Vertex pomocVtx;
            if (!(a.getY() < b.getY())) {
                pomocVtx = a;
                a = b;
                b = pomocVtx;
            }
            if (!(b.getY() < c.getY())) {
                pomocVtx = b;
                b = c;
                c = pomocVtx;
            }
        }
        for (int y = (int) a.getY(); y < b.getY(); y++) {
            if (y >= 0 && y <= height - 1) {

                double s1 = Math.max((y - a.getY()) / (b.getY() - a.getY()), 0);
                double s2 = Math.max((y - a.getY()) / (c.getY() - a.getY()), 0);
                Vertex vab = lerp.lerp(a, b, s1);
                Vertex vac = lerp.lerp(a, c, s2);

                if (vab.getX() > vac.getX()) {
                    Vertex temp = vab;
                    vab = vac;
                    vac = temp;
                }

                for (int x = (int) Math.max(vab.getX(), 0); x < Math.min(vac.getX(), width - 1); x++) {
                    if (x >= 0 && x <= width - 1) {
                        double t = (x - vab.getX()) / (vac.getX() - vab.getX());

                        vab.setColor(lerpColor(a.getColor(),b.getColor(),s1));
                        vac.setColor(lerpColor(a.getColor(),c.getColor(),s2));

                        if (t >= 0) {
                            Vertex z = lerp.lerp(vab, vac, t);
                            Col WallCol;
                            if (oneColorTriangle) {
                                WallCol = lerpColor(vab.getColor(), vac.getColor(), t);
                            } else {
                                WallCol = color;
                            }
                            zBuffer.compareZ(x, y, z.getZ(), WallCol);
                        }
                    }
                }
            }
        }
        for (int y = (int) Math.max(b.getY(), 0); y < Math.min(c.getY(), height - 1); y++) {
            if (y >= 0 && y <= height - 1) {

                double s1 = Math.max((y - b.getY()) / (c.getY() - b.getY()), 0);
                double s2 = Math.max((y - a.getY()) / (c.getY() - a.getY()), 0);
                Vertex vbc = lerp.lerp(b, c, s1);
                Vertex vac = lerp.lerp(a, c, s2);
                if (vbc.getX() > vac.getX()) {
                    Vertex temp = vbc;
                    vbc = vac;
                    vac = temp;
                }

                for (int x = (int) Math.max(vbc.getX(), 0); x < Math.min(vac.getX(), width - 1); x++) {
                    if (x >= 0 && x <= width - 1) {
                        double t = (x - vbc.getX()) / (vac.getX() - vbc.getX());

                        vbc.setColor(lerpColor(b.getColor(), c.getColor(), s1));
                        vac.setColor(lerpColor(a.getColor(), c.getColor(), s2));

                        if (t >= 0) {
                            Vertex z = lerp.lerp(vbc, vac, t);
                            Col WallCol;
                            if (oneColorTriangle) {
                                WallCol = lerpColor(vbc.getColor(), vac.getColor(), t);
                            } else {
                                WallCol = color;
                            }
                            zBuffer.compareZ(x, y, z.getZ(), WallCol);
                        }
                    }
                }
            }
        }
    }

    public Col lerpColor(Col c1, Col c2, double t) {
        //t = Math.max(0, Math.min(1, t)); // Omezíme t do rozsahu [0,1]

        double r = (1 - t) * c1.r + t * c2.r;
        double g = (1 - t) * c1.g + t * c2.g;
        double b = (1 - t) * c1.b + t * c2.b;

        return new Col(r, g, b);
    }
}