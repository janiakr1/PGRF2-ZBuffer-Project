package raster;

import Utils.Lerp;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;
import transforms.Vec3D;

public class LineRasterizer {

    ImageBuffer img;
    private final ZBuffer zBuffer;
    private final int width,height;
    private final Lerp<Vertex> lerp;
    public LineRasterizer(ZBuffer zBuffer)
    {
        this.zBuffer = zBuffer;
        this.img = zBuffer.getRaster();
        this.width = zBuffer.getWidth();
        this.height = zBuffer.getHeight();
        this.lerp = new Lerp<>();
    }

    public void rasterize(Vertex p1, Vertex p2, Col col){

        Vertex a = p1.dehomog();
        Vertex b = p2.dehomog();

        a.setResolution(width,height);
        b.setResolution(width,height);

        a = a.transformToWindow(a.getPosition());
        b = b.transformToWindow(b.getPosition());

        if(a.getY() > b.getY()){
            Vertex temp = a;
            a = b;
            b = temp;
        }

        float k = (float) ((float) (b.getY() - a.getY()) / (b.getX() - a.getX())); // tangens (úhel směrnice)
        float q = (float) (a.getY() - k * a.getX());

        if (a.getX() == b.getX()) {
            if (b.getY() > a.getY()) {      // dolní vertikální úsečka
                for (int y = (int) a.getY(); y <= b.getY(); y++) {
                    if(y > 0 && y < height) {
                        double s = (y - a.getY()) / (b.getY() - a.getY());
                        Vertex z = lerp.lerp(a,b,s);
                        zBuffer.compareZ((int) a.getX(),y,z.getZ(),col);
                    }
                }

            } else {            // horní vertikální úsečka
                for (int y = (int) b.getY(); y <= a.getY(); y++) {
                    if(y > 0 && y < height) {
                        double s = (y - b.getY()) / (a.getY() - b.getY());
                        Vertex z = lerp.lerp(b,a,s);
                        zBuffer.compareZ((int) a.getX(),y,z.getZ(),col);
                    }
                }
            }
        } else {
            if (k <= 1 && k >= -1) {
                if (a.getX() > b.getX()) { // prohození pořadí proměných, aby x1 bylo menší
                    Vertex temp = a;
                    a = b;
                    b = temp;

                }
                for (int x = (int) a.getX(); x <= b.getX(); x++) {
                    if(x >= 0 && x < width) {
                        int y = Math.round(k * x + q);
                        if(y > 0 && y < height && x > 0 && x < width) {
                            double s = (y - a.getY()) / (b.getY() - a.getY());
                            Vertex z = lerp.lerp(a,b,s);
                            zBuffer.compareZ(x,y,z.getZ(),col);
                        }
                    }
                }
            } else {
                if (a.getY() > b.getY()) {

                    Vertex temp = a;
                    a = b;
                    b = temp;
                }
                for (int y = (int) a.getY(); y <= b.getY(); y++) {
                    int x = Math.round((y - q) / k);
                    if(y > 0 && y < height && x > 0 && x < width) {
                        double s = (y - a.getY()) / (b.getY() - a.getY());
                        Vertex z = lerp.lerp(a,b,s);
                        zBuffer.compareZ(x,y,z.getZ(),col);
                    }
                }
            }
        }
    }
}
