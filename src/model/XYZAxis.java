package model;

import transforms.Col;

public class XYZAxis extends Solid{
    public XYZAxis(){
        getVertexBuffer().add(new Vertex(10,0,0,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(-10,0,0,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(0,10,0,new Col(0x00FF00)));
        getVertexBuffer().add(new Vertex(0,-10,0,new Col(0x00FF00)));
        getVertexBuffer().add(new Vertex(0,0,10,new Col(0x0000FF)));
        getVertexBuffer().add(new Vertex(0,0,-10,new Col(0x0000FF)));

        //Index buffer
        //Lines
        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        getIndexBuffer().add(2);
        getIndexBuffer().add(3);

        getIndexBuffer().add(4);
        getIndexBuffer().add(5);



        // Part buffer
        getPartBuffer().add(new Part(TopologyType.LINE,0,3));
    }
}
