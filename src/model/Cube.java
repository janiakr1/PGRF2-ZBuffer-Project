package model;

import transforms.Col;

public class Cube extends Solid {
    public Cube(){
        getVertexBuffer().add(new Vertex(-1,-1,-1,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(1,-1,-1,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(1,1,-1,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(-1,1,-1,new Col(0xFFFF00)));

        getVertexBuffer().add(new Vertex(-1,-1,1,new Col(0xFF0000)));
        getVertexBuffer().add(new Vertex(1,-1,1,new Col(0xFF00FF)));
        getVertexBuffer().add(new Vertex(1,1,1,new Col(0x0000FF)));
        getVertexBuffer().add(new Vertex(-1,1,1,new Col(0xFF0000)));

        //Lines
        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        getIndexBuffer().add(1);
        getIndexBuffer().add(2);

        getIndexBuffer().add(2);
        getIndexBuffer().add(3);

        getIndexBuffer().add(0);
        getIndexBuffer().add(3);

        getIndexBuffer().add(0);
        getIndexBuffer().add(4);

        getIndexBuffer().add(1);
        getIndexBuffer().add(5);

        getIndexBuffer().add(2);
        getIndexBuffer().add(6);

        getIndexBuffer().add(3);
        getIndexBuffer().add(7);

        getIndexBuffer().add(4);
        getIndexBuffer().add(5);

        getIndexBuffer().add(5);
        getIndexBuffer().add(6);

        getIndexBuffer().add(6);
        getIndexBuffer().add(7);

        getIndexBuffer().add(7);
        getIndexBuffer().add(4);

        //Triangles
        getIndexBuffer().add(0);
        getIndexBuffer().add(1);
        getIndexBuffer().add(2);

        getIndexBuffer().add(0);
        getIndexBuffer().add(2);
        getIndexBuffer().add(3);

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);
        getIndexBuffer().add(4);

        getIndexBuffer().add(1);
        getIndexBuffer().add(4);
        getIndexBuffer().add(5);

        getIndexBuffer().add(1);
        getIndexBuffer().add(2);
        getIndexBuffer().add(5);

        getIndexBuffer().add(2);
        getIndexBuffer().add(5);
        getIndexBuffer().add(6);

        getIndexBuffer().add(2);
        getIndexBuffer().add(3);
        getIndexBuffer().add(6);

        getIndexBuffer().add(3);
        getIndexBuffer().add(7);
        getIndexBuffer().add(6);

        getIndexBuffer().add(0);
        getIndexBuffer().add(3);
        getIndexBuffer().add(4);

        getIndexBuffer().add(3);
        getIndexBuffer().add(4);
        getIndexBuffer().add(7);

        getIndexBuffer().add(4);
        getIndexBuffer().add(5);
        getIndexBuffer().add(6);

        getIndexBuffer().add(6);
        getIndexBuffer().add(7);
        getIndexBuffer().add(4);

        getPartBuffer().add(new Part(TopologyType.LINE,0,12));
        getPartBuffer().add(new Part(TopologyType.TRIANGLE,24,12));
    }
}
