package model;

import transforms.Col;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec3D;

public class Vertex implements Vectorizable<Vertex> {
    private Point3D position;
    private Col color;
    private int width;
    private int height;

    public Vertex(double x, double y, double z, Col color) {
        this.position = new Point3D(x,y,z);
        this.color = color;
    }

    public Vertex(Point3D position, Col color) {
        this.position = position;
        this.color = color;
    }

    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    public void setColor(Col color) {
         this.color = color;
    }

    @Override
    public Vertex mul(double d) {
       return new Vertex(getPosition().mul(d),getColor());
    }

    public void setResolution(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public double getX(){
        return getPosition().x;
    }
    public double getY(){
        return getPosition().y;
    }
    public double getZ(){
        return getPosition().z;
    }

    @Override
    public Vertex add(Vertex v) {
        return new Vertex(getPosition().add(new Point3D(v.getPosition())),getColor());
    }

    public Vertex dehomog() {
        return new Vertex(new Point3D(getPosition().dehomog()),getColor());
    }

    public Vertex transform(Mat4 mat) {
        return new Vertex(getPosition().mul(mat),getColor());
    }

    public Vertex transformToWindow(Point3D p)
    {
        return new Vertex(new Point3D(p.ignoreW()
                .mul(new Vec3D(1,-1,1))
                .add(new Vec3D(1,1,0))
                .mul(new Vec3D((width-1)/2,(height-1)/2,1))),color);
    }
}
