package model;

import transforms.Mat4;
import transforms.Mat4Identity;

import java.util.ArrayList;
import java.util.List;

public class Scene {
    List<Solid> solids;
    List<Mat4> modelMats;

    public Scene(){
        solids = new ArrayList<>();
        modelMats = new ArrayList<>();
    }

    public List<Solid> getSolids() {
        return solids;
    }

    public List<Mat4> getModelMats() {
        return modelMats;
    }

    public void addSolid(Solid solid){
        solids.add(solid);
        modelMats.add(new Mat4Identity());
    }

    public void setModelMat(int index, Mat4 rotx){
        modelMats.set(index,rotx);
    }
}
