package model;

import transforms.Col;

public class Pyramid extends Solid {
    public Pyramid() {
        //Vertex buffer
        getVertexBuffer().add(new Vertex(3,3,1,new Col(0xFF0000))); //v1
        getVertexBuffer().add(new Vertex(-1,-1,-1,new Col(0x00FF00))); //v2
        getVertexBuffer().add(new Vertex(1,1,-1,new Col(0xFF00FF))); //v3
        getVertexBuffer().add(new Vertex(1,-1,-1,new Col(0x00FFFF))); //v4

        //Index buffer
        //Triangles
        getIndexBuffer().add(0);
        getIndexBuffer().add(1);
        getIndexBuffer().add(2);

        getIndexBuffer().add(0);
        getIndexBuffer().add(2);
        getIndexBuffer().add(3);

        getIndexBuffer().add(1);
        getIndexBuffer().add(2);
        getIndexBuffer().add(3);

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);
        getIndexBuffer().add(3);


        //Lines
        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        getIndexBuffer().add(0);
        getIndexBuffer().add(2);

        getIndexBuffer().add(0);
        getIndexBuffer().add(3);

        getIndexBuffer().add(1);
        getIndexBuffer().add(2);

        getIndexBuffer().add(1);
        getIndexBuffer().add(3);

        getIndexBuffer().add(2);
        getIndexBuffer().add(3);




        // Part buffer
        getPartBuffer().add(new Part(TopologyType.LINE,12,6));
        getPartBuffer().add(new Part(TopologyType.TRIANGLE,0,4));
    }
}